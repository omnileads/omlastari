import pika
import os
import time
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class RabbitMQManager:
    MAX_RETRIES = 3
    RETRY_DELAY = 5  

    def __init__(self):
        self.rabbitmq_host = os.getenv('RABBITMQ_HOSTNAME', 'rabbitmq')        
        self.connection = None
        self.channel = None
        self.connect()  # Esta función conectará y creará el canal

    def connect(self):
        for _ in range(self.MAX_RETRIES):
            try:
                connection_params = pika.ConnectionParameters(
                    host=self.rabbitmq_host,
                    heartbeat=300,  
                    blocked_connection_timeout=900
                )
                self.connection = pika.BlockingConnection(connection_params)
                self.channel = self.connection.channel()
                break
            except pika.exceptions.AMQPError as e:
                logger.warning(f"Error connecting to RabbitMQ: {e}. Retrying...")
                time.sleep(self.RETRY_DELAY)

    def is_connected(self):
        return self.connection and self.connection.is_open

    def publish_message(self, queue_name, message, expiration_time):
        for _ in range(self.MAX_RETRIES):
            try:
                self.channel.queue_declare(queue=queue_name, durable=True)

                properties = pika.BasicProperties(expiration=str(expiration_time * 1000))  # tiempo de expiración en milisegundos
                self.channel.basic_publish(
                    exchange='', 
                    routing_key=queue_name,
                    body=message,
                    properties=properties  # agregamos las propiedades aquí
                )
                print(f" [x] Sent {message}")
                return True  # Se añade un retorno de True indicando éxito
            except pika.exceptions.AMQPError as e:
                print(f"An error occurred: {e}. Retrying...")
                time.sleep(self.RETRY_DELAY)
        return False  # Se añade un retorno de False luego de agotar las reintentos


    def close_connection(self):
        try:
            if self.connection and self.connection.is_open:
                self.connection.close()
        except Exception as e:
            logger.error(f"Error closing connection: {e}")
