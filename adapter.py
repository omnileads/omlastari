import websocket
import ari
from ari import ARI
#import rel
import os
import json
import sys 
import logging
import redis
import signal
import traceback 
from rabbitmq_manager import RabbitMQManager
from redis_manager import RedisManager

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

# Obtain environment variables within this method
ASTERISK_USER = os.getenv('ARI_USER', 'default_user')
ASTERISK_PASS = os.getenv('ARI_PASS', 'default_pass')
ASTERISK_HOST = os.getenv('ARI_HOST', 'asterisk')
ASTERISK_PORT = os.getenv('ARI_PORT', '7088')
ASTERISK_APP = os.getenv('ASTERISK_APP', 'Queue')


class CallManager:
    def __init__(self):
        self.bridge_id = None
        self.ari = ARI()
        self.rabbitmq_manager = RabbitMQManager()
        self.redis_manager = RedisManager() 

    def client(self):
        try:
            ari_client = ari.connect(f'http://{ASTERISK_HOST}:{ASTERISK_PORT}/', ASTERISK_USER, ASTERISK_PASS)
            return ari_client
        except Exception as e:
            logging.error(f"Error setting up ARI client: {str(e)}")
            logging.error(traceback.format_exc()) 
            return None       

    def handle_stasis_start(self, event): 
        
        try:        
            args = event.get('args', [])
            id_camp = None 
            # if args:
            #     id_camp = args[0]  # Asumiendo que "Queue" es el primer argumento
            #     custom_arg = args[1] if len(args) > 1 else None  # "nombre_argumento" es el segundo argumento

            # Aqui levanto variables que llegan desde Asterisk Dialplan Stasis(Queue)
            channel_id = event.get('channel', {}).get('id')
            self.phone_number = event.get('channel', {}).get('caller', {}).get('number')
            self.did_number = event.get('channel', {}).get('dialplan', {}).get('exten')
            self.context = event.get('channel', {}).get('dialplan', {}).get('context')

            # Me traigo el hash de INR para este DID
            did_info = self.redis_manager.get_hash_as_dict(f"OML:INR:{self.did_number}")
        
            if did_info:
                # Aquí, si did_info no está vacío, significa que el DID está definido en tu sistema
                logging.info(f"Info for DID {self.did_number}: {did_info}")                
            else:
                # Si es que no hay DID en Redis, entonces no prosigo con la llamada
                logging.warning(f"DID {self.did_number} not defined in the system.")
                self.ari.playback(channel_id, 'congestion')            
                self.ari.hangup_channel(channel_id)
                return

            # Consultar Redis para obtener información sobre la campaña
            dst = did_info.get('DST').split(',')
            self.id_camp = dst[1]
            self.camp_info = self.redis_manager.get_hash_as_dict(f"OML:CAMP:{self.id_camp}")

            # Busco si hay tengo un id_cliente relacionado al telefono que esta llamando
            self.id_customer = self.redis_manager.get_id_by_phone(self.phone_number)            

            # Chequeo si la llamada llegar por from-pstn
            if  self.context == 'from-pstn':
                self.handle_pstn_channel(event, self.id_camp)                
            else:
                self.handle_agent_channel(event)
        except Exception as e:
            logging.error(f"Error handling stasis start: {str(e)}")    

    def handle_pstn_channel(self, event, id_camp):
        logging.info(f"********* PSTN INBOUND Received Message: {event}")
        channel_id = event['channel']['id'] 

        try:
            # Creamos el bridge cuando recibimos la primera llamada desde la PSTN           
            bridge = self.ari.create_bridge()
            if bridge is not None and 'id' in bridge:
                self.bridge_id = bridge.get('id')                     
            else:
                logging.error("Failed to create bridge or 'id' not present in the response.") 

            # atiendo el canal entrante
            self.ari.answer(channel_id)        
            
            if not isinstance(channel_id, str):
                logging.error(f"Unexpected type for channel_id: {type(channel_id)}")
                return

            if self.bridge_id is None:
                logging.error("Bridge is not created")
                return

            if not hasattr(self, "client"):
                logging.error("self.client is not configured")
                return                    
            
            logging.info(f"phone_number: {self.phone_number} -- id_customer: {self.id_customer}")
            logging.info(f"channel_id: {channel_id} -- id_campaign: {self.id_camp} -- channel_id: {channel_id}")

            # Publicar mensaje a RabbitMQ usando modulo rabbitmq_manager.py  
            # Genero metada sobre la llamada para enviar como mensaje a rabbitMQ
            message_dict = {
                'id_channel': channel_id,
                'id_campaign': self.id_camp,
                'id_bridge': self.bridge_id,
                'id_customer': self.id_customer,
                'phone_number': self.phone_number
            }
            message_json = json.dumps(message_dict)
            queue_name = f"tel_queue_{self.id_camp}"
            queue_timeout = 30

            if not self.rabbitmq_manager.is_connected():
                logging.warning("Reconnecting to RabbitMQ...")
                self.rabbitmq_manager.connect()

            result = self.rabbitmq_manager.publish_message(queue_name, message_json, queue_timeout)
            
            if not result:
                 raise Exception("Failed to publish message to RabbitMQ")

        except Exception as e:
            logging.error(f"Error handling PSTN channel: {str(e)}")


    def handle_agent_channel(self, event):
        logging.info(f"********* AGENT Channel Received Message: {event}")
        channel_id = event['channel']['id'  ] 
        
        try: 
            self.ari.playback(channel_id, 'beep')
            
        except Exception as e:
            logging.error(f"Error handling AGENT channel: {str(e)}")


    def handle_stasis_end(self, event):
        channel_id = event.get('channel', {}).get('id')

        if not channel_id:
            logging.error("StasisEnd event without channel ID")
            return

        # Intenta colgar el canal que desencadenó el evento StasisEnd
        try:
            self.ari.hangup_channel(channel_id)
        except Exception as e:
            logging.error(f"Error hanging up channel {channel_id}: {e}")


    def handle_dial(self, event):
        dialstatus = event['dialstatus']
        logging.info(f"****** DIAL ag channel Event - Status: {dialstatus} ********")

call_manager = CallManager()
    
def on_message(ws, message):
    #logging.info(f"Received Message: {message}")
    event_to_dict = json.loads(message)
    event = event_to_dict.get('type', 'default')

    if event == 'StasisStart':
        call_manager.handle_stasis_start(event_to_dict)
    elif event == 'Dial':
        call_manager.handle_dial(event_to_dict)
    elif event == 'StasisEnd':
        call_manager.handle_stasis_end(event_to_dict)

def on_error(ws, error):
    logging.info("***** ERROR *****")

def on_close(ws, close_status_code, close_msg):
    logging.info("Closed connection")

def on_open(ws):
    logging.info("Opened connection")


if __name__ == "__main__":
    
    # Obtenemos las variables de entorno
    ASTERISK_USER = os.getenv('ARI_USER', 'default_user')
    ASTERISK_PASS = os.getenv('ARI_PASS', 'default_pass')
    ASTERISK_HOST = os.getenv('ARI_HOST', 'asterisk')
    ASTERISK_PORT = os.getenv('ARI_PORT', '7088')
    ASTERISK_APP = os.getenv('ASTERISK_APP', 'Queue')

    # Creamos la URI del WebSocket utilizando las variables
    ws_uri = f"ws://{ASTERISK_HOST}:{ASTERISK_PORT}/ari/events"
    ws_uri += f"?api_key={ASTERISK_USER}:{ASTERISK_PASS}&app={ASTERISK_APP}"

    # Configuración del WebSocket
    ws = websocket.WebSocketApp(
        ws_uri,
        on_open=on_open,
        on_message=on_message,
        on_error=on_error,
        on_close=on_close
    )

    # Función para manejar la señal de cierre
    def signal_handler(signum, frame):
        logging.info("Signal received, closing connection")
        ws.close()

    # Registro de la señal de interrupción
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    # Iniciar el loop de eventos del WebSocket
    ws.run_forever()