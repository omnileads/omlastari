import pika
import ari
from ari import ARI
import json
import os
import logging
import redis
import time
import random

logging.basicConfig(level=logging.INFO)

ASTERISK_HOST = os.getenv('ARI_HOST', 'asterisk')
ASTERISK_PORT = os.getenv('ARI_PORT', '7088')
ASTERISK_USER = os.getenv('ARI_USER', 'default_user')
ASTERISK_PASS = os.getenv('ARI_PASS', 'default_pass')
RABBITMQ_HOST = os.getenv('RABBITMQ_HOSTNAME', 'localhost')

redis_conn = redis.Redis(
    host=os.getenv('REDIS_HOST', 'redis'),
    port=os.getenv('REDIS_PORT', '6379'),
    db=0,
    decode_responses=True
)


def process_message(ari_client, ch, method, properties, body):
    
    logging.info(f"Received message: {body}")
    message_data = json.loads(body.decode("utf-8"))

    # Medata de la llamada en cola RabbitMQ
    id_channel_pstn = message_data.get('id_channel')
    id_campaign = message_data.get('id_campaign')
    id_bridge = message_data.get('id_bridge')
    id_customer = message_data.get('id_customer')
    phone_number = message_data.get('phone_number')

    logging.info(f"******* CONSUMER : *** id_channel ***: {id_channel_pstn}, *** id_campaign ***: {id_campaign}, *** id_bridge ***: {id_bridge}")
    logging.info(f"******* CONSUMER : *** id_customer ***: {id_customer}, *** phone_number ***: {phone_number}")
    
    if id_channel_pstn and id_campaign:
        try:                        
            # Reproducir MOH sobre el channel PSTN
            ari_client.start_moh(id_channel_pstn)

            pstn_channel = ari_client.get_channel_details(id_channel_pstn)
            pstn_channel_state = pstn_channel['state']
            
            if pstn_channel_state != 'Up':
                logging.warning(f"Channel state is not 'Up'. Current state: {pstn_channel_state}")
                return

            agent_endpoint = None
            while not agent_endpoint:  # Mientras no haya un agente disponible, seguimos intentando
                # Obtener un agente disponible para la campaña
                agent_endpoint = get_available_agent_for_campaign(id_campaign)                
                sip_agent = agent_endpoint[0]
                id_agent = agent_endpoint[1]    

                if not agent_endpoint:
                    logging.info("No agents available, waiting for one to become ready...")
                    time.sleep(5)

            pstn_channel = ari_client.get_channel_details(id_channel_pstn)
            pstn_channel_state = pstn_channel['state']
            
            if pstn_channel_state != 'Up':
                logging.warning(f"Channel state is not 'Up'. Current state: {pstn_channel_state}")
                return
            
            app_args_str = f"{id_campaign},{id_channel_pstn},{id_bridge},{id_customer},{phone_number},{id_agent}"
            # Generar el originate hacia el endpoint del agente
            ari_client.originate_channel(endpoint=sip_agent, app='Deliver', appArgs=app_args_str)
            

        except Exception as e:
            logging.error(f"Failed to originate call: {e}")

    else:
        logging.warning("Received message missing 'idchannel_pstn' or 'id_campaign'")

    ch.basic_ack(delivery_tag=method.delivery_tag)


def get_available_agent_for_campaign(id_campaign):
    agents_key = f"OML:CAMP:{id_campaign}:AGENTS"    
    agents = redis_conn.smembers(agents_key)
    
    logging.info(f"Agents for {agents_key}: {agents}")    

    if agents:
        ready_agents = []
        # Itera sobre los IDs de agentes y recupera sus detalles de Redis
        for agent_id in agents:
            agent_data = redis_conn.hgetall(f"OML:AGENT:{agent_id}")
            status = agent_data.get('STATUS', '') 
            if status == 'READY':
                sip = agent_data.get('SIP')
                ready_agents.append((sip, agent_id))  # Guarda como tupla (sip, agent_id)

        if ready_agents:
            # Selecciona un agente al azar de la lista de agentes listos
            selected_agent_sip, selected_agent_id = random.choice(ready_agents)
            # Puedes usar selected_agent_id como desees ahora.
            #return f"PJSIP/{selected_agent_sip}", selected_agent_id
            return f"PJSIP/{selected_agent_sip}", selected_agent_id
        else:
            logging.warning(f"No ready agents available for campaign {id_campaign}")
            return None, None
    else:
        logging.warning(f"No agents assigned for campaign {id_campaign}")
        return None, None


def get_campaign_ids_from_set(redis_conn):
    try:
        # Directamente obtenemos todos los id_campaign que implican Queues
        campaign_ids = redis_conn.smembers("OML:QUEUE:CAMP")
        campaign_ids = [id.decode("utf-8") if isinstance(id, bytes) else id for id in campaign_ids]

        return campaign_ids

    except Exception as e:
        print(f"Error al recuperar los IDs de las campañas: {str(e)}")
        return []


def setup_queue(channel, queue_name, ari_client):
    channel.queue_declare(queue=queue_name, durable=True)
    channel.basic_qos(prefetch_count=1)
    on_message_callback = lambda ch, method, properties, body: process_message(ari_client, ch, method, properties, body)
    channel.basic_consume(queue=queue_name, on_message_callback=on_message_callback)


def main():
    connection = None
    try:
        # Inicializo el cliente ari.py
        ari_client = ARI(user=ASTERISK_USER, password=ASTERISK_PASS, host=ASTERISK_HOST, port=ASTERISK_PORT)

        connection = pika.BlockingConnection(pika.ConnectionParameters(host=RABBITMQ_HOST))
        channel = connection.channel()
    
        #campaign_ids = get_campaign_ids(redis_conn)
        campaign_ids = ["2", "3"]  

        for campaign_id in campaign_ids:
            queue_name = f"tel_queue_{campaign_id}"
            setup_queue(channel, queue_name, ari_client)

        logging.info('Starting to consume messages from RabbitMQ')
        channel.start_consuming()

    except Exception as e:
        logging.error(f"An error occurred: {e}")
    finally:
        if connection:
            connection.close()

if __name__ == "__main__":
    main()
